package com.vra.core.user.response.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.vra.core.sql.dto.AbstractJpaDto;
import com.vra.core.user.enums.UserType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(exclude = "token")
@JsonInclude(JsonInclude.Include.ALWAYS)
public class UserResponseDto extends AbstractJpaDto {

	private String firstName;

	private String lastName;

	private String email;

	private String mobile;

	private int mobileVerified;

	private String profilePicture;

	private String countryCode;

	private String isoCode;

	private String username;

	private int mfaEnabled;

	private int emailVerified;

	private int subscription;

	private String token;

	private UserType userType;

}