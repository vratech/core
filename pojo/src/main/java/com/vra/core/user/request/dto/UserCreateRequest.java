package com.vra.core.user.request.dto;

import com.vra.core.user.enums.UserType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author naveen
 *
 * @date 23-Dec-2018
 */
@Getter
@Setter
@ToString(exclude = "password")
public class UserCreateRequest {

	private String firstName;

	private String lastName;

	private String email;

	private String mobile;

	private int countryCode;

	private String username;

	private String password;

	private UserType userType;

	private String createdBy;

	private int mfaEnabled;

	private int subscription;

}