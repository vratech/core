package com.vra.core.user.request.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by mayank.pruthi on 16/02/17.
 */
@Getter
@Setter
@ToString(exclude = "password", callSuper = true)
public class UserLoginRequestDto extends LoginRequestDto {

	private String username;

	private String password;

	private String mobile;

	private Integer countryCode;

	private String isoCode;

}