package com.vra.core.user.enums;

public enum UserType {

	STUDENT,
	FACULTY,
	ADMIN,
	PARENT,
	ALUMINI
}