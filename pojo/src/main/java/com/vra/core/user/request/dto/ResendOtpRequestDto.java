package com.vra.core.user.request.dto;

import com.vra.core.user.enums.UserType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class ResendOtpRequestDto {

	private String username;

	private String mobile;

	private Integer countryCode;

	private UserType userType;
}