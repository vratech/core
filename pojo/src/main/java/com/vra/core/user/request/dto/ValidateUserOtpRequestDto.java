package com.vra.core.user.request.dto;

import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
public class ValidateUserOtpRequestDto extends LoginRequestDto {

	private String username;

	private String mobile;

	private Integer countryCode;

	@NotBlank(message = "OTP can't be empty")
	private String otp;

}