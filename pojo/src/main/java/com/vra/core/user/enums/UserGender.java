/**
 * 
 */
package com.vra.core.user.enums;

/**
 * @author naveen
 *
 * @date 05-Feb-2018
 */
public enum UserGender {

	Male,
	Female,
	Transgender,
	Other
}