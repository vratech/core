package com.vra.core.user.request.dto;

import com.vra.core.user.enums.UserType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LoginRequestDto {

	protected UserType userType;

	protected String visitorId;

	protected String os;

	protected String osVerison;

	protected String browser;

	protected String browserVersion;

	protected String ip;

	protected String imei;

	protected String device;

	protected String model;

	protected String applicationId;

	protected String fingerprint;

	protected String payload;

	protected String signature;

}