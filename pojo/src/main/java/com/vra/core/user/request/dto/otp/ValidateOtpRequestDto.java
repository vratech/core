package com.vra.core.user.request.dto.otp;


import javax.validation.constraints.NotBlank;

import com.vra.core.user.enums.UserType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString(callSuper = true)
@NoArgsConstructor
public class ValidateOtpRequestDto extends GenerateOtpRequestDto {

	@NotBlank(message = "otp can't be blank")
	private String otp;
	
	public ValidateOtpRequestDto(String otp, String mobile, String countryCode, UserType userType) {
		super(mobile, countryCode, userType);
		this.otp = otp;
	}

}
