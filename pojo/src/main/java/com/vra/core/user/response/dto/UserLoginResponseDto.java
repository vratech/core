package com.vra.core.user.response.dto;

import com.vra.core.user.enums.UserType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author naveen
 *
 * @date 28-Oct-2018
 */
@Getter
@Setter
@ToString
public class UserLoginResponseDto {

	private String firstName;

	private String lastName;

	private String email;

	private String mobile;

	private String countryCode;

	private UserType userType;

	private String username;
	
	private String userId;

	private String token;

}