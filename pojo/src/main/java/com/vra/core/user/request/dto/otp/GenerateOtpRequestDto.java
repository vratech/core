package com.vra.core.user.request.dto.otp;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.vra.core.user.enums.UserType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Setter
@Getter
@NoArgsConstructor
public class GenerateOtpRequestDto {

	private String userId;

	@Pattern(regexp = "([0-9]+)", message = "Invalid mobile")
	@NotBlank(message = "mobile can't be blank")
	private String mobile;

	@Pattern(regexp = "([0-9]+)", message = "Invalid countryCode")
	@NotBlank(message = "countryCode can't be blank")
	private String countryCode;

	@NotNull(message = "userType may not be null")
	private UserType userType;

	public GenerateOtpRequestDto(String mobile, String countryCode, UserType userType) {
		super();
		this.mobile = mobile;
		this.countryCode = countryCode;
		this.userType = userType;
	}
	
	
	
}