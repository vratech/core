/**
 * 
 */
package com.vra.core.sql.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author naveen
 *
 * @date 28-Oct-2018
 */
@Getter
@Setter
@ToString
public abstract class AbstractJpaDto {

	protected String id;

	protected Date createdAt;

	protected String createdBy;

	protected Date updatedAt;

	protected String updatedBy;

	protected boolean deleted;

}