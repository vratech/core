package com.vra.core.sqljpa;

import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

/**
 * 
 * @author naveen
 *
 * @date 23-Dec-2018
 */
public class CustomJpaVendor extends HibernateJpaVendorAdapter {

	private final HibernateJpaDialect jpaDialect = new CustomJpaDialect();

	@Override
	public HibernateJpaDialect getJpaDialect() {
		return this.jpaDialect;
	}
}