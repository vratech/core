package com.vra.core.sqljpa.annotation;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import com.vra.core.sqljpa.DbContextHolder;
import com.vra.core.sqljpa.enums.DbType;

@Aspect
@Component
public class WriteConnectionInterceptor implements Ordered {

	private static final Logger logger = LogManager.getLogger(WriteConnectionInterceptor.class);

	private int order;

	@Value("20")
	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public int getOrder() {
		return order;
	}

	@Pointcut(value = "execution(public * *(..))")
	public void anyPublicMethod() {
	}

	@Around("@annotation(writeConnection)")
	public Object proceed(ProceedingJoinPoint pjp, WriteConnection writeConnection) throws Throwable {
		try {
			logger.debug(Thread.currentThread().getName() + ": In Write Only Connection");
			DbContextHolder.setDbType(DbType.MASTER);
			return pjp.proceed();

		} finally {
			// restore state
			DbContextHolder.clearDbType();
		}
	}
}