package com.vra.core.sqljpa.specification.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import com.vra.core.sqljpa.entity.AbstractJpaEntity;
import com.vra.core.sqljpa.specification.CasaSpecification;

public class CasaSpecificationBuilder<T extends AbstractJpaEntity> {

	private final List<SearchCriteria> params;

	public CasaSpecificationBuilder() {
		params = new ArrayList<>();
	}

	public CasaSpecificationBuilder<T> with(String key, CriteriaOperation operation, Object value) {

		params.add(new SearchCriteria(key, operation, value));
		return this;
	}

	public Specification<T> build() {

		if (params.isEmpty()) {
			return null;
		}

		List<Specification<T>> specs = addParamsToSpecification();

		return generateFinalSpecificationFromList(specs);
	}

	private Specification<T> generateFinalSpecificationFromList(List<Specification<T>> specs) {

		Specification<T> result = specs.get(0);

		for (int i = 1; i < specs.size(); i++) {
			result = Specification.where(result).and(specs.get(i));
		}

		return result;
	}

	private List<Specification<T>> addParamsToSpecification() {

		List<Specification<T>> specs = new ArrayList<>();

		for (SearchCriteria param : params) {
			specs.add(new CasaSpecification<T>(param));
		}

		return specs;
	}
}