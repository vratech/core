package com.vra.core.sqljpa.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Abstract JPA class with generic ID. All models will be extending this class
 * 
 */
@Getter
@Setter
@ToString
@EntityListeners({ CasaEntityListener.class })
@MappedSuperclass
@SuppressWarnings("serial")
public abstract class AbstractJpaEntity implements Serializable {

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(columnDefinition = "BINARY(16)")
	protected UUID id;

	@Column(name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date createdAt;

	@Column(name = "created_by", nullable = true)
	protected String createdBy;

	@Column(name = "updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date updatedAt;

	@Column(name = "updated_by", nullable = true)
	protected String updatedBy;

	@Column(name = "is_deleted", columnDefinition = "bit(1) default 0")
	protected boolean deleted = false;

	public AbstractJpaEntity(
			UUID id,
			Date createdAt,
			String createdBy,
			Date updatedAt,
			String updatedBy,
			boolean deleted) {

		super();
		this.id = id;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.updatedAt = updatedAt;
		this.updatedBy = updatedBy;
		this.deleted = deleted;
	}

	public AbstractJpaEntity() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		AbstractJpaEntity other = (AbstractJpaEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}

		return true;
	}

}