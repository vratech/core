package com.vra.core.sqljpa.annotation;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import com.vra.core.sqljpa.DbContextHolder;
import com.vra.core.sqljpa.enums.DbType;

@Aspect
@Component
public class ReadOnlyConnectionInterceptor implements Ordered {

	private static final Logger logger = LogManager.getLogger(ReadOnlyConnectionInterceptor.class);

	private int order;

	@Value("20")
	public void setOrder(int order) {
		this.order = order;
	}

	@Override
	public int getOrder() {
		return order;
	}

	@Pointcut(value = "execution(public * *(..))")
	public void anyPublicMethod() {
	}

	@Around("@annotation(readOnlyConnection)")
	public Object proceed(ProceedingJoinPoint pjp, ReadOnlyConnection readOnlyConnection) throws Throwable {
		try {
			logger.debug(Thread.currentThread().getName() + ": In Read Only Connection");
			DbContextHolder.setDbType(DbType.REPLICA);
			Object result = pjp.proceed();
			DbContextHolder.clearDbType();
			return result;
		} finally {
			// restore state
			DbContextHolder.clearDbType();
		}
	}
}