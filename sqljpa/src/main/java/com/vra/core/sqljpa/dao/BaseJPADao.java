package com.vra.core.sqljpa.dao;

import java.util.List;
import java.util.Map;

import com.vra.core.sqljpa.entity.AbstractJpaEntity;

/**
 * Base DAO for JPA core operations with multiple end points (CRUD operations)
 */
public interface BaseJPADao<T> {

	/**
	 * DAO method to select a JPA entity based on given IDx
	 * 
	 * @param clazz
	 *            - entity class to be selected
	 * @param id
	 *            - id of element to be selected
	 * @return JPA entity selected
	 */
	T selectEntity(Class<? extends AbstractJpaEntity> clazz, long id);

	/**
	 * DAO method to select a JPA entity based on given search parameters
	 * 
	 * @param clazz
	 *            - entity class to be selected
	 * @param paramMap
	 *            - parameter map to perform search
	 * @return JPA entity selected
	 */
	T selectEntity(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap);

	/**
	 * DAO method to select a JPA entity based on given search parameters
	 * 
	 * @param clazz
	 *            - entity class to be selected
	 * @param paramMap
	 *            - parameter map to perform search
	 * @param groupMap
	 *            - parameter map to perform group by action
	 * @return JPA entity selected
	 */
	T selectEntity(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap, Map<String, String> groupMap);

	/**
	 * DAO method to select a JPA entity based on given search parameters
	 * 
	 * @param clazz
	 *            - entity class to be selected
	 * @param paramMap
	 *            - parameter map to perform search
	 * @param groupMap
	 *            - parameter map to perform group by action
	 * @param orderMap
	 *            - parameter map to order the result set
	 * @return JPA entity selected
	 */
	T selectEntity(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap, Map<String, String> groupMap, Map<String, String> orderMap);

	/**
	 * DAO method to select all JPA entities for given type
	 * 
	 * @param clazz
	 *            - entity type to be selected
	 * @return all entities of given type
	 */
	List<T> selectEntities(Class<? extends AbstractJpaEntity> clazz);

	/**
	 * DAO method to select all JPA entities of given type matching the filter criteria
	 * 
	 * @param clazz
	 *            - entity type to be selected
	 * @param paramMap
	 *            - parameters to perform search/filter
	 * @return list of JPA entities matching filter criteria
	 */
	List<T> selectEntities(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap);

	/**
	 * DAO method to select the JPA entities of given type matching the filter criteria, grouped by group columns & limited to given limit in given order
	 * 
	 * @param clazz
	 *            - entity type to be selected
	 * @param paramMap
	 *            - parameters to perform filter/search
	 * @param groupMap
	 *            - parameters to group by columns
	 * @param orderMap
	 *            - parameters to order the result
	 * @return
	 */
	List<T> selectEntities(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap, Map<String, String> groupMap, Map<String, String> orderMap);

	/**
	 * DAO method to select all JPA entities of given type matching the filter criteria and in given order
	 * 
	 * @param clazz
	 *            - entity type to be selected
	 * @param paramMap
	 *            - parameters to perform search/filter
	 * @param orderMap
	 *            - parameters to order the result
	 * @return list of JPA entities matching filter criteria and in given order
	 */
	List<T> selectEntities(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap, Map<String, String> orderMap);

	/**
	 * DAO method to select the JPA entities of given type matching the filter criteria and limited to given limit
	 * 
	 * @param clazz
	 *            - entity type to be selected
	 * @param paramMap
	 *            - parameters to perform filter/search
	 * @param limit
	 *            - number of records to be selected
	 * @return list of JPA entities matching filter criteria
	 */
	List<T> selectEntities(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap, int limit);

	/**
	 * DAO method to select the JPA entities of given type matching the filter criteria, grouped by group columns & limited to given limit in given order
	 * 
	 * @param clazz
	 *            - entity type to be selected
	 * @param paramMap
	 *            - parameters to perform filter/search
	 * @param groupMap
	 *            - parameters to group by columns
	 * @param orderMap
	 *            - parameters to order the result
	 * @param page
	 *            - page number to get records from
	 * @param limit
	 *            - number of records to be selected
	 * @return list of JPA entities matching filter criteria and in given order
	 */
	List<T> selectEntities(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap, Map<String, String> groupMap, Map<String, String> orderMap, int page, int limit);

}