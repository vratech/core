package com.vra.core.sqljpa.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.vra.core.sqljpa.entity.AbstractJpaEntity;

public interface AbstractJpaService<T extends AbstractJpaEntity, I extends Serializable> {

	T save(T entity);

	T saveAndFlush(T entity);

	T update(T entity);

	T updateAndFlush(T entity);

	List<T> save(List<T> entities);

	List<T> saveAndFlush(List<T> entities);

	List<T> update(List<T> entities);

	List<T> updateAndFlush(List<T> entities);

	long count();

	T find(I id);

	List<T> find(List<I> ids);

	List<T> findAll();

	List<T> findAll(Sort sort);

	Page<T> findAll(Pageable pageable);

	List<T> findList(List<I> ids);

	List<T> findAllList();

	T findById(I id);

	T findByIdAndDeleted(I id, boolean deleted);

	List<T> findByIdIn(List<I> ids);

	List<T> findByIdInAndDeleted(List<I> ids, boolean deleted);

	void flush();

	void delete(T entity);

	void delete(List<T> entities);

	void delete(I id);

	void deleteAll();

}