package com.vra.core.sqljpa;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public class RoutingDataSource extends AbstractRoutingDataSource {

	private static final Logger log = LogManager.getLogger(RoutingDataSource.class);

	@Override
	protected Object determineCurrentLookupKey() {

		if (log.isTraceEnabled()) {
			log.trace("is readonly transaction: " + TransactionSynchronizationManager.isCurrentTransactionReadOnly());
		}

		return DbContextHolder.getDbType();
	}
}