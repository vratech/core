package com.vra.core.sqljpa.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.vra.core.sqljpa.entity.AbstractJpaEntity;

@NoRepositoryBean
public interface AbstractJpaRepository<T extends AbstractJpaEntity, I extends Serializable> extends JpaRepository<T, I> {

	Optional<T> findById(I id);

	T findByIdAndDeleted(I id, boolean deleted);

	List<T> findByIdIn(List<I> ids);

	List<T> findByIdInAndDeleted(List<I> ids, boolean deleted);
}