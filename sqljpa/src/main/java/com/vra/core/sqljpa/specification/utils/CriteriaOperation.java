package com.vra.core.sqljpa.specification.utils;

public enum CriteriaOperation {

	TRUE("true"),
	FALSE("false"),
	EQ("="),
	GTE(">="),
	GT(">"),
	LTE("<="),
	LT("<"),
	DATE_EQ("="),
	DATE_GTE(">="),
	DATE_GT(">"),
	DATE_LTE("<="),
	DATE_LT("<"),
	IN("in"),
	REGEXP("regexp"),
	FIND_IN_SET("find_in_set");

	CriteriaOperation(String op) {
		operation = op;
	}

	private String operation;

	public String getOperation() {
		return operation;
	}

}