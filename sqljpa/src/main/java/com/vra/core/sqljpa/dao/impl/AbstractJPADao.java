package com.vra.core.sqljpa.dao.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.vra.core.base.common.CSVConverter;
import com.vra.core.sqljpa.dao.BaseJPADao;
import com.vra.core.sqljpa.entity.AbstractJpaEntity;

/**
 * SQL Dao class with all basic methods (CRUD operations).
 */
public abstract class AbstractJPADao<T extends AbstractJpaEntity> implements BaseJPADao<T> {

	private static final Logger logger = LogManager.getLogger(AbstractJPADao.class);

	public abstract EntityManager getEntityManager();

	@SuppressWarnings("unchecked")
	@Override
	public T selectEntity(Class<? extends AbstractJpaEntity> clazz, long id) {
		T entity = null;
		try {
			entity = (T) getEntityManager().find(clazz, id);
		} catch (NoResultException e) {
			logger.warn("No data found with id: [" + id + "] in entity: " + clazz.getSimpleName());
		}
		return entity;
	}

	@Override
	public T selectEntity(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap) {
		return selectEntity(clazz, paramMap, null, null);
	}

	@Override
	public T selectEntity(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap, Map<String, String> groupMap) {
		return selectEntity(clazz, paramMap, groupMap, null);
	}

	@Override
	public T selectEntity(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap, Map<String, String> groupMap, Map<String, String> orderMap) {
		if (paramMap == null || paramMap.isEmpty()) {
			return null;
		}

		List<T> entities = selectEntities(clazz, paramMap, groupMap, orderMap, 0, 1);

		return CollectionUtils.isNotEmpty(entities) ? entities.get(0) : null;
	}

	@Override
	public List<T> selectEntities(Class<? extends AbstractJpaEntity> clazz) {
		return selectEntities(clazz, null, null, null, 0, -1);
	}

	@Override
	public List<T> selectEntities(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap) {
		return selectEntities(clazz, paramMap, null, null, 0, -1);
	}

	@Override
	public List<T> selectEntities(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap, Map<String, String> groupMap, Map<String, String> orderMap) {
		return selectEntities(clazz, paramMap, groupMap, orderMap, 0, -1);
	}

	@Override
	public List<T> selectEntities(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap, Map<String, String> orderMap) {
		return selectEntities(clazz, paramMap, null, orderMap, 0, -1);
	}

	@Override
	public List<T> selectEntities(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap, int limit) {
		return selectEntities(clazz, paramMap, null, null, 0, limit);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> selectEntities(Class<? extends AbstractJpaEntity> clazz, Map<String, Object> paramMap, Map<String, String> groupMap, Map<String, String> orderMap, int page, int limit) {
		try {
			StringBuilder queryBuilder = new StringBuilder();

			queryBuilder.append("SELECT t FROM ").append(clazz.getName()).append(" t ");

			queryBuilder.append(createWhereClause(paramMap));
			queryBuilder.append(createGroupClause(groupMap));
			queryBuilder.append(createOrderClause(orderMap));

			logger.debug("Query: " + queryBuilder.toString());
			// Create query object
			Query queryObject = getEntityManager().createQuery(queryBuilder.toString(), clazz);

			setPaginationParams(queryObject, page, limit);

			logger.debug("JAI MATA DI");
			return queryObject.getResultList();
		} catch (Exception e) {
			logger.error("Error occurred while selecting entties of type: " + clazz + " : ", e);
		}
		return new ArrayList<>();
	}

	protected void setPaginationParams(Query queryObject, int page, int limit) {
		if (limit > 0) {
			if (page > 0) {
				queryObject.setFirstResult((page - 1) * limit);
			}
			queryObject.setMaxResults(limit);
		}
	}

	protected String createOrderClause(Map<String, String> orderMap) {
		StringBuilder queryBuilder = new StringBuilder();

		if (orderMap != null && !orderMap.isEmpty()) {

			boolean isFirstTime = true;
			for (Entry<String, String> orderEntry : orderMap.entrySet()) {
				if (isFirstTime) {
					queryBuilder.append(" ORDER BY ");
					isFirstTime = false;
				} else {
					queryBuilder.append(", ");
				}

				queryBuilder.append(orderEntry.getKey()).append(" ").append(orderEntry.getValue());
			}
		}

		return queryBuilder.toString();
	}

	protected String createGroupClause(Map<String, String> groupMap) {
		StringBuilder queryBuilder = new StringBuilder();

		if (groupMap != null && !groupMap.isEmpty()) {

			boolean isFirstTime = true;

			for (String groupKey : groupMap.keySet()) {

				if (isFirstTime) {
					queryBuilder.append(" GROUP BY ");
					isFirstTime = false;
				} else {
					queryBuilder.append(", ");
				}

				queryBuilder.append(groupKey);
			}
		}

		return queryBuilder.toString();
	}

	protected String createWhereClause(Map<String, Object> paramMap) {
		StringBuilder queryBuilder = new StringBuilder();

		if (paramMap != null && !paramMap.isEmpty()) {

			boolean isFirstTime = true;
			for (Entry<String, Object> paramEntry : paramMap.entrySet()) {
				if (isFirstTime) {
					queryBuilder.append(" WHERE ");
					isFirstTime = false;
				} else {
					queryBuilder.append(" AND ");
				}

				queryBuilder.append(paramEntry.getKey());
				Object value = paramEntry.getValue();
				if (value instanceof String || value instanceof Date || value instanceof LocalDate) {
					queryBuilder.append(" = '").append(value).append("'");
				} else if (value instanceof List) {
					queryBuilder.append(getListParams(value));
				} else {
					queryBuilder.append(" = ").append(value);
				}
			}
		}
		return queryBuilder.toString();
	}

	@SuppressWarnings("unchecked")
	private String getListParams(Object value) {

		List<Object> values = (List<Object>) value;

		if (values.isEmpty()) {
			throw new IllegalArgumentException("Empty list passed as query argument");
		}

		List<Long> longList = new ArrayList<>();
		List<String> stringList = new ArrayList<>();

		for (Object object : values) {
			if (NumberUtils.isCreatable(object.toString())) {
				longList.add(NumberUtils.toLong(object.toString()));
			} else {
				stringList.add(object.toString());
			}
		}

		StringBuilder queryBuilder = new StringBuilder();

		if (!longList.isEmpty()) {
			queryBuilder.append(" IN (").append(CSVConverter.getCSVString(longList)).append(")");
		} else {
			queryBuilder.append(" IN (").append(CSVConverter.getCSVString(stringList.toArray(new String[0]))).append(")");
		}

		return queryBuilder.toString();
	}

}