package com.vra.core.sqljpa.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.vra.core.sqljpa.entity.AbstractJpaEntity;
import com.vra.core.sqljpa.repository.AbstractJpaRepository;
import com.vra.core.sqljpa.service.AbstractJpaService;

public abstract class AbstractJpaServiceImpl<T extends AbstractJpaEntity, I extends Serializable, R extends AbstractJpaRepository<T, I>>
		implements AbstractJpaService<T, I> {

	protected abstract R getJpaRepository();

	@Override
	public T save(T entity) {
		return getJpaRepository().save(entity);
	}

	@Override
	public T saveAndFlush(T entity) {
		return getJpaRepository().saveAndFlush(entity);
	}

	@Override
	public T update(T entity) {
		return getJpaRepository().save(entity);
	}

	@Override
	public T updateAndFlush(T entity) {
		return getJpaRepository().saveAndFlush(entity);
	}

	@Override
	public List<T> save(List<T> entities) {
		return getJpaRepository().saveAll(entities);
	}

	@Override
	public List<T> saveAndFlush(List<T> entities) {
		List<T> savedEntities = getJpaRepository().saveAll(entities);
		getJpaRepository().flush();

		return savedEntities;
	}

	@Override
	public List<T> update(List<T> entities) {
		return getJpaRepository().saveAll(entities);
	}

	@Override
	public List<T> updateAndFlush(List<T> entities) {
		List<T> updatedEntities = getJpaRepository().saveAll(entities);
		getJpaRepository().flush();

		return updatedEntities;
	}

	@Override
	public long count() {
		return getJpaRepository().count();
	}

	@Override
	public T find(I id) {
		return getJpaRepository().findById(id).orElse(null);
	}

	@Override
	public List<T> find(List<I> ids) {
		return getJpaRepository().findAllById(ids);
	}

	@Override
	public List<T> findAll() {
		return getJpaRepository().findAll();
	}

	@Override
	public List<T> findAll(Sort sort) {
		return getJpaRepository().findAll(sort);
	}

	@Override
	public Page<T> findAll(Pageable pageable) {
		return getJpaRepository().findAll(pageable);
	}

	@Override
	public List<T> findList(List<I> ids) {
		return getJpaRepository().findAllById(ids);
	}

	@Override
	public T findById(I id) {
		return getJpaRepository().findById(id).orElse(null);
	}

	@Override
	public T findByIdAndDeleted(I id, boolean deleted) {
		return getJpaRepository().findByIdAndDeleted(id, deleted);
	}

	@Override
	public List<T> findByIdIn(List<I> ids) {
		return getJpaRepository().findByIdIn(ids);
	}

	@Override
	public List<T> findByIdInAndDeleted(List<I> ids, boolean deleted) {
		return getJpaRepository().findByIdInAndDeleted(ids, deleted);
	}

	@Override
	public void flush() {
		getJpaRepository().flush();
	}

	@Override
	public void delete(T entity) {
		getJpaRepository().delete(entity);
	}

	@Override
	public void delete(List<T> entities) {
		getJpaRepository().deleteAll(entities);
	}

	@Override
	public void delete(I id) {
		getJpaRepository().deleteById(id);
	}

	@Override
	public void deleteAll() {
		getJpaRepository().deleteAll();
	}
}