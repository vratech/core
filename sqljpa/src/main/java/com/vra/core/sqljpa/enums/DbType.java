package com.vra.core.sqljpa.enums;

public enum DbType {

	MASTER, REPLICA

}