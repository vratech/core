package com.vra.core.utilservice.annotations.validator;

import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.vra.core.utilservice.annotations.Enumeration;

public class EnumerationValidator implements ConstraintValidator<Enumeration, Object> {

	private Set<String> stringList;
	private Set<Integer> integerList;
	private boolean isRoomType;

	@SuppressWarnings("rawtypes")
	public void initialize(Enumeration constraint) {
		Class<? extends Enum<?>> enumSelected = constraint.enumClass();

		// if (enumSelected == RoomType.class) {
		// isRoomType = true;
		// integerList = new HashSet<>();
		// RoomType[] roomTypes = RoomType.values();
		// for (RoomType roomType : roomTypes) {
		// integerList.add(roomType.getRoomTypeId());
		// }
		// } else {
		stringList = new HashSet<>();
		for (Enum anEnum : enumSelected.getEnumConstants()) {
			stringList.add(anEnum.name());
		}
		// }
	}

	public boolean isValid(Object obj, ConstraintValidatorContext context) {
		if (obj != null) {
			if (obj instanceof Integer && isRoomType) {
				return integerList.contains(obj);
			} else if (!isRoomType && obj instanceof String) {
				return stringList.contains(obj);
			}
		}
		return true;
	}
}