/**
 * 
 */
package com.vra.core.kafkaclient.utils;

import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.core.env.Environment;

import lombok.experimental.UtilityClass;

/**
 * @author naveen
 *
 * @date 16-Oct-2018
 */
@UtilityClass
public class KafkaUtil {

	private static final Logger logger = LogManager.getLogger(KafkaUtil.class);

	public static Properties getProducerProperties(Environment environment) {
		Properties prop = new Properties();

		prop.put(KafkaConstants.BOOTSTRAP_SERVERS, getProperty(environment, KafkaConstants.BOOTSTRAP_SERVERS));
		prop.put(KafkaConstants.Producer.KEY_SERIALIZER, getProperty(environment, KafkaConstants.Producer.KEY_SERIALIZER));
		prop.put(KafkaConstants.Producer.VALUE_SERIALIZER, getProperty(environment, KafkaConstants.Producer.VALUE_SERIALIZER));
		prop.put(KafkaConstants.Producer.REQUEST_REQUIRED_ACKS, getProperty(environment, KafkaConstants.Producer.REQUEST_REQUIRED_ACKS));
		prop.put(KafkaConstants.Producer.NUMBER_OF_PARTITIONS, getProperty(environment, KafkaConstants.Producer.NUMBER_OF_PARTITIONS));

		return prop;
	}

	public static Properties getConsumerProperties(Environment environment) {
		Properties properties = new Properties();

		properties.put(KafkaConstants.BOOTSTRAP_SERVERS, getProperty(environment, KafkaConstants.BOOTSTRAP_SERVERS));
		properties.put(KafkaConstants.Consumer.KEY_DESERIALIZER, getProperty(environment, KafkaConstants.Consumer.KEY_DESERIALIZER));
		properties.put(KafkaConstants.Consumer.VALUE_DESERIALIZER, getProperty(environment, KafkaConstants.Consumer.VALUE_DESERIALIZER));
		properties.put(KafkaConstants.Consumer.GROUP_ID, getProperty(environment, KafkaConstants.Consumer.GROUP_ID));
		properties.put(KafkaConstants.Consumer.ENABLE_AUTO_COMMIT, getProperty(environment, KafkaConstants.Consumer.ENABLE_AUTO_COMMIT));
		properties.put(KafkaConstants.Consumer.AUTO_COMMIT_INTERVAL_MS, getProperty(environment, KafkaConstants.Consumer.AUTO_COMMIT_INTERVAL_MS));
		properties.put(KafkaConstants.Consumer.SESSION_TIMEOUT_MS, getProperty(environment, KafkaConstants.Consumer.SESSION_TIMEOUT_MS));
		properties.put(KafkaConstants.Consumer.MAX_PARTITION_FETCH_BYTES, 21943040);

		return properties;
	}

	private static String getProperty(Environment environment, String key) {
		String value = environment.getProperty(key);
		logger.debug("Loading Key => " + key + ", value => " + value);
		return value;
	}

}