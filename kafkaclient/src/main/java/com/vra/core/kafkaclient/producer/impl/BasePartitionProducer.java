/**
 * 
 */
package com.vra.core.kafkaclient.producer.impl;

/**
 * 
 * @author naveen
 *
 * @date 27-Oct-2018
 */
public abstract class BasePartitionProducer<K, V> extends BaseProducer<K, V> {

	/**
	 * Method to publish record on Kafka on a specific partition
	 * 
	 * @param topic
	 * @param payload
	 * @param partition
	 */
	public abstract void publish(String topic, Object payload, int partition);

}