package com.vra.core.kafkaclient.producer;

import org.springframework.core.env.Environment;

public interface Publisher {

	void configure(Environment environment);

}