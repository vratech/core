package com.vra.core.kafkaclient.web.servlet;

import java.util.Set;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

public class ServletPlugin implements ServletContainerInitializer {

	@Override
	public void onStartup(Set<Class<?>> arg0, ServletContext ctx) throws ServletException {
		System.out.println("ConsumerShutdownServlet Initialized");
		ctx.addServlet(ConsumerShutdownServlet.class.getName(), new ConsumerShutdownServlet()).addMapping(ConsumerShutdownServlet.URL);
	}

}