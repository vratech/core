package com.vra.core.kafkaclient.producer.impl;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.springframework.core.env.Environment;

import com.vra.core.kafkaclient.producer.Publisher;
import com.vra.core.kafkaclient.utils.KafkaUtil;

public abstract class BaseProducer<K, V> implements Publisher {

	protected Producer<K, V> producer = null;

	public abstract void publish(String topic, Object payload);

	@Override
	public void configure(Environment environment) {
		this.producer = new KafkaProducer<>(KafkaUtil.getProducerProperties(environment));
	}

}