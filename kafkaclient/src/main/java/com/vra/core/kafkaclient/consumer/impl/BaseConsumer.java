package com.vra.core.kafkaclient.consumer.impl;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.core.env.Environment;

import com.vra.core.kafkaclient.consumer.Consumer;
import com.vra.core.kafkaclient.utils.KafkaUtil;

public abstract class BaseConsumer<K, V> implements Consumer<K, V> {

	private static final Logger logger = LogManager.getLogger(BaseConsumer.class);

	@SuppressWarnings("rawtypes")
	protected static final List<BaseConsumer> consumers = new CopyOnWriteArrayList<>();

	private KafkaConsumer<K, V> consumer;
	private AtomicBoolean shutdown;
	private CountDownLatch shutdownLatch;

	public abstract List<String> getTopics();

	public abstract int getPollDuration();

	public BaseConsumer() {
		consumers.add(this);
	}

	@Override
	public void configure(Environment environment) {
		this.consumer = new KafkaConsumer<>(KafkaUtil.getConsumerProperties(environment));
		this.consumer.subscribe(getTopics());
		this.shutdown = new AtomicBoolean(false);
		this.shutdownLatch = new CountDownLatch(1);
	}

	@Override
	public void run() {
		try {
			while (!shutdown.get()) {
				ConsumerRecords<K, V> records = this.consumer.poll(getPollDuration());

				if (records != null && !records.isEmpty()) {

					subscribe(records);
					logger.info(getTopics() + " size : " + records.count() + " processed");

					commit();
				}
			}
		} catch (Exception e) {
			logger.error("Exception while parsing json : ", e);
		} finally {
			consumer.close();
			shutdownLatch.countDown();
			logger.info(this.getClass().getName() + " Consumer exited");
		}
	}

	private void commit() {
		try {
			this.consumer.commitSync();
		} catch (Exception e) {
			logger.error("commit api get failed : ", e);
		}
	}

	public boolean shutdown() throws InterruptedException {
		// Check if shutdown is already in process
		if (shutdownLatch.getCount() == 0) {
			return false;
		}
		shutdown.set(true);
		shutdownLatch.await();
		return true;
	}

	@SuppressWarnings("rawtypes")
	public static List<BaseConsumer> getConsumers() {
		return consumers;
	}

}