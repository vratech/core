package com.vra.core.security.validation;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vra.core.security.dto.CurrentUser;

public interface RequestValidator {

	CurrentUser validate(HttpServletRequest request, HttpServletResponse response, CurrentUser user) throws IOException;
}