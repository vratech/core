package com.vra.core.security.utils;

import java.nio.charset.Charset;
import java.util.Base64;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vra.core.base.exception.CoreException;
import com.vra.core.security.dto.UsernamePasswordDto;

import lombok.experimental.UtilityClass;

@UtilityClass
public class BasicAuthenticationUtil {

	private static final Logger logger = LogManager.getLogger(BasicAuthenticationUtil.class);

	public static UsernamePasswordDto buildUsernamePasswordDto(final String authorization) {
		String username = null;
		String password = null;

		if (authorization != null && authorization.startsWith("Basic")) {
			String base64Credentials = authorization.substring("Basic".length()).trim();
			String credentials = new String(Base64.getDecoder().decode(base64Credentials), Charset.forName("UTF-8"));
			// credentials = username:password
			final String[] values = credentials.split(":", 2);

			if (values.length == 2) {
				username = values[0];
				password = values[1];

				logger.debug(username);
				logger.debug(password);
			}
		}

		if (username == null || password == null) {
			throw new CoreException("Invalid Username and Password");
		}

		return new UsernamePasswordDto(username, password);
	}

}