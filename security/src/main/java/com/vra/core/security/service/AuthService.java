package com.vra.core.security.service;

import com.vra.core.base.common.ResponseDto;
import com.vra.core.user.request.dto.ResendOtpRequestDto;
import com.vra.core.user.request.dto.UserLoginRequestDto;
import com.vra.core.user.request.dto.ValidateUserOtpRequestDto;
import com.vra.core.user.response.dto.UserLoginResponseDto;
import com.vra.core.user.response.dto.UserResponseDto;

public interface AuthService {

	ResponseDto<UserLoginResponseDto> login(UserLoginRequestDto dto);

	ResponseDto<UserLoginResponseDto> validateOtp(ValidateUserOtpRequestDto dto);

	ResponseDto<Void> resendOtp(ResendOtpRequestDto dto);

	ResponseDto<UserResponseDto> validateToken(String token);

	ResponseDto<Void> logout();

	ResponseDto<UserResponseDto> getCurrentUser();

	void validateUrlPermission(String userId, String uri);

}