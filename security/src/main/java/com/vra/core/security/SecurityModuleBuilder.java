package com.vra.core.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vra.core.security.helper.UserMicroserviceHelper;
import com.vra.core.security.interceptor.AuthInterceptor;
import com.vra.core.security.service.AuthService;
import com.vra.core.security.service.impl.AuthServiceImpl;
import com.vra.core.security.validation.RequestValidator;
import com.vra.core.security.validation.TokenAuthenticationValidator;
import com.vra.core.security.validation.UrlAuthorizationValidator;
import com.vra.core.user.enums.UserType;

public class SecurityModuleBuilder {

	private ObjectMapper objectMapper;

	private boolean corsSupport = false;

	private String authServiceUrl;

	private String userServiceUrl;

	private boolean enableUrlBasedAuthorization = false;

	List<RequestValidator> validators = new ArrayList<>();
	List<UserType> supportedUserTypes = new ArrayList<>();

	public SecurityModuleBuilder objectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
		return this;
	}

	public SecurityModuleBuilder corsSupport(boolean corsSupport) {
		this.corsSupport = corsSupport;
		return this;
	}

	public SecurityModuleBuilder authServiceUrl(String authServiceUrl) {
		this.authServiceUrl = authServiceUrl;
		return this;
	}

	public SecurityModuleBuilder userServiceUrl(String userServiceUrl) {
		this.userServiceUrl = userServiceUrl;
		return this;
	}

	public SecurityModuleBuilder enableUrlBasedAuthorization(boolean enableUrlBasedAuthorization) {
		this.enableUrlBasedAuthorization = enableUrlBasedAuthorization;
		return this;
	}

	public SecurityModuleBuilder addValidator(RequestValidator validator) {
		Assert.notNull(validator, "validator can't be null");

		validators.add(validator);
		return this;
	}

	public SecurityModuleBuilder supportedUserTypes(List<UserType> supportedUserTypes) {
		Assert.notNull(supportedUserTypes, "supportedUserTypes can't be null");

		this.supportedUserTypes = supportedUserTypes;
		return this;
	}

	public AuthInterceptor build() {
		Assert.notNull(objectMapper, "ObjectMapper can't be null");
		Assert.hasText(authServiceUrl, "Auth Service URL can't be null");
		Assert.hasText(userServiceUrl, "User Service URL can't be null");

		UserMicroserviceHelper.configure(authServiceUrl, userServiceUrl);

		AuthService authService = new AuthServiceImpl(objectMapper);

		validators.add(0, new TokenAuthenticationValidator(authService, supportedUserTypes));

		if (enableUrlBasedAuthorization) {
			validators.add(1, new UrlAuthorizationValidator(authService));
		}

		AuthInterceptor authInterceptor = new AuthInterceptor(objectMapper, validators);
		authInterceptor.setCorsSupport(corsSupport);

		return authInterceptor;

	}

}