package com.vra.core.security.validation;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vra.core.base.common.ResponseDto;
import com.vra.core.base.constants.CoreSecurityConstants;
import com.vra.core.base.exception.CoreException;
import com.vra.core.base.utils.SecureCookieUtil;
import com.vra.core.security.dto.CurrentUser;
import com.vra.core.security.service.AuthService;
import com.vra.core.user.enums.UserType;
import com.vra.core.user.response.dto.UserResponseDto;

public class TokenAuthenticationValidator implements RequestValidator {

	private AuthService authService;
	private List<UserType> supportedUserTypes;

	private static final Logger logger = LogManager.getLogger(TokenAuthenticationValidator.class);

	public TokenAuthenticationValidator(AuthService authService, List<UserType> supportedUserTypes) {
		this.authService = authService;
		this.supportedUserTypes = supportedUserTypes;
	}

	@Override
	public CurrentUser validate(HttpServletRequest request, HttpServletResponse response, CurrentUser user) throws IOException {
		String token = request.getHeader(CoreSecurityConstants.TOKEN_HEADER_NAME);

		Cookie[] cookies = request.getCookies();

		if (StringUtils.isBlank(token) && cookies != null) {
			logger.debug("Token Not found in Request Header. Now searching in Cookies");

			for (Cookie cookie : cookies) {

				if (CoreSecurityConstants.TOKEN_HEADER_NAME.equals(cookie.getName()) || CoreSecurityConstants.AUTH_TOKEN_HEADER_NAME.equals(cookie.getName())) {
					token = cookie.getValue();
					break;
				}
			}
		}

		logger.debug("Auth Requested For Token: " + token);

		if (StringUtils.isNotBlank(token)) {
			ResponseDto<UserResponseDto> responseDto = authService.validateToken(token);

			if (responseDto.isStatus()) {
				UserResponseDto dto = responseDto.getData();
				logger.debug("User Fetched after Authentication: " + dto);

				checkUserType(dto);

				request.setAttribute("userId", dto.getId());
				response.addCookie(SecureCookieUtil.create(CoreSecurityConstants.TOKEN_HEADER_NAME, token));

				return CurrentUser.builder()
						.token(token)
						.userId(dto.getId())
						.username(dto.getUsername())
						.firstName(dto.getFirstName())
						.lastName(dto.getLastName())
						.mobile(dto.getMobile())
						.countryCode(dto.getCountryCode())
						.userType(dto.getUserType())
						.build();

			}
		}

		throw new CoreException("Invalid token or User Session has expired");
	}

	private void checkUserType(UserResponseDto dto) {
		if (!supportedUserTypes.isEmpty() && !supportedUserTypes.contains(dto.getUserType())) {
			throw new CoreException("Invalid token or User Session has expired");
		}
	}

}