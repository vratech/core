package com.vra.core.security.helper;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import lombok.experimental.UtilityClass;

@UtilityClass
public class UserMicroserviceHelper {

	private static final Logger logger = LogManager.getLogger(UserMicroserviceHelper.class);

	private static Map<String, String> apiUrls = new HashMap<>();

	public static void configure(String authServiceUrl, String userServiceUrl) {

		logger.info("authServiceUrl: " + authServiceUrl);
		logger.info("userServiceUrl: " + userServiceUrl);

		apiUrls.put(USER_LOGIN, userServiceUrl + "/user/login");
		apiUrls.put(LOGIN_OTP_VALIDATION, userServiceUrl + "/user/otp/validate");
		apiUrls.put(RESEND_LOGIN_OTP, userServiceUrl + "/user/otp/resend");
		apiUrls.put(GET_CURRENT_USER_PROFILE, userServiceUrl + "/user/getProfile");
		apiUrls.put(SIGN_OUT, userServiceUrl + "/user/logout");

		apiUrls.put(VALIDATE_TOKEN, authServiceUrl + "/user/authenticate");
		apiUrls.put(VALIDATE_PERMISSION, authServiceUrl + "/user/authorize");

	}

	public static final String USER_LOGIN = "USER_LOGIN";
	public static final String LOGIN_OTP_VALIDATION = "LOGIN_OTP_VALIDATION";
	public static final String RESEND_LOGIN_OTP = "RESEND_LOGIN_OTP";
	public static final String GET_CURRENT_USER_PROFILE = "GET_CURRENT_USER_PROFILE";
	public static final String SIGN_OUT = "SIGN_OUT";

	public static final String VALIDATE_TOKEN = "VALIDATE_TOKEN";
	public static final String VALIDATE_PERMISSION = "VALIDATE_PERMISSION";

	public static String getUrl(String api) {
		return apiUrls.get(api);
	}
}