package com.vra.core.security.dto;

import com.vra.core.user.enums.UserType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
public class CurrentUser {

	private String token;
	private String userId;
	private String firstName;
	private String lastName;
	private String mobile;
	private String countryCode;
	private UserType userType;
	private String username;

}