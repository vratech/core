package com.vra.core.security.validation;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vra.core.base.exception.CoreException;
import com.vra.core.security.dto.CurrentUser;
import com.vra.core.security.service.AuthService;

public class UrlAuthorizationValidator implements RequestValidator {

	private static final Logger logger = LogManager.getLogger(UrlAuthorizationValidator.class);

	private AuthService authService;

	public UrlAuthorizationValidator(AuthService authService) {
		this.authService = authService;
	}

	@Override
	public CurrentUser validate(HttpServletRequest request, HttpServletResponse response, CurrentUser user) throws IOException {

		authService.validateUrlPermission(user.getUserId(), getValidationUri(request));

		return user;
	}

	private String getValidationUri(HttpServletRequest request) {
		String requestedUri = request.getHeader("uri");
		String accessUri = request.getRequestURI();

		if (requestedUri != null) {
			requestedUri = requestedUri.toLowerCase();
		}

		if (accessUri != null) {
			accessUri = accessUri.toLowerCase();
		}

		if (requestedUri == null) {
			return accessUri;

		} else {

			if (accessUri != null && accessUri.startsWith(requestedUri)) {
				return requestedUri;
			}

			logger.error("Trying to access: " + accessUri + " while asking for permission of: " + requestedUri + " permission denying");
			throw new CoreException("Trying to access: " + accessUri + " while asking for permission of: " + requestedUri + " permission denying");
		}
	}

}