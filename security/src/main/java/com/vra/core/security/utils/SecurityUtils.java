/**
 * 
 */
package com.vra.core.security.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vra.core.security.context.SecurityContextHolder;
import com.vra.core.security.dto.CurrentUser;

import lombok.experimental.UtilityClass;

/**
 * @author naveen
 *
 * @date 31-Dec-2018
 */
@UtilityClass
public class SecurityUtils {

	private static final Logger logger = LogManager.getLogger(SecurityUtils.class);

	public static String getCurrentUserName() {

		CurrentUser currentUser = getCurrentUser();

		String userName = "";

		if (currentUser != null) {
			userName = currentUser.getFirstName() + " " + currentUser.getLastName();
		}

		return userName;
	}

	public static String getCurrentUserId() {
		CurrentUser currentUser = getCurrentUser();

		String userId = "";

		if (currentUser != null) {
			userId = currentUser.getUserId();
		}

		return userId;
	}

	public static String getCurrentUserToken() {
		CurrentUser currentUser = getCurrentUser();

		String token = "";

		if (currentUser != null) {
			token = currentUser.getToken();
		}

		return token;
	}

	public static CurrentUser getCurrentUser() {
		CurrentUser currentUser = null;

		try {
			currentUser = SecurityContextHolder.getCurrentUser();
		} catch (Exception e) {
			logger.error("Error while fetching current user from security context holder: ", e);
		}
		return currentUser;
	}
}