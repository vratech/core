package com.vra.core.security.interceptor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vra.core.base.common.ResponseDto;
import com.vra.core.base.exception.CoreException;
import com.vra.core.base.exception.CoreHttpException;
import com.vra.core.security.context.SecurityContextHolder;
import com.vra.core.security.dto.CurrentUser;
import com.vra.core.security.validation.RequestValidator;

public class AuthInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LogManager.getLogger(AuthInterceptor.class);

	private boolean corsSupport;
	private ObjectMapper objectMapper;
	private List<RequestValidator> validations;

	public AuthInterceptor(@NotNull ObjectMapper objectMapper) {
		this(objectMapper, new ArrayList<>());
	}

	public AuthInterceptor(@NotNull ObjectMapper objectMapper, @NotNull List<RequestValidator> validations) {
		Assert.notNull(validations, "validations can't be null");
		this.objectMapper = objectMapper;
		this.validations = validations;
	}

	public void setCorsSupport(boolean corsSupport) {
		this.corsSupport = corsSupport;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {

		if (corsSupport && HttpMethod.OPTIONS.name().equals(request.getMethod())) {
			logger.debug("CORS request!");
			return true;
		}

		try {
			CurrentUser user = null;

			for (RequestValidator validator : validations) {
				user = validator.validate(request, response, user);
			}

			SecurityContextHolder.setCurrentUser(user);
			return true;

		} catch (CoreHttpException ex) {
			ResponseDto<Void> res = new ResponseDto<>();
			res.setMessage("We are facing some internal issue, please try after sometime.");

			PrintWriter writer = response.getWriter();
			writer.print(objectMapper.writeValueAsString(res));

			response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
			response.setHeader("Content-Type", "application/json");
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			writer.close();

		} catch (CoreException ex) {
			returnError(request, response, ex);
		}

		return false;
	}

	private void returnError(HttpServletRequest request, HttpServletResponse response, CoreException ex) throws IOException {
		String message = ex != null ? ex.getMessage() : "Auth Token Is Invalid";

		ResponseDto<Void> res = new ResponseDto<>(false, message);

		if (ex != null) {
			response.setStatus(ex.getStatusCode());
		}

		response.setHeader("Content-Type", "application/json");
		response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));

		try (PrintWriter writer = response.getWriter()) {
			writer.print(objectMapper.writeValueAsString(res));
		}
	}

}