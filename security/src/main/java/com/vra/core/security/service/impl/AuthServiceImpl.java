package com.vra.core.security.service.impl;

import java.io.IOException;

import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vra.core.base.common.CoreHttpClient;
import com.vra.core.base.common.ResponseDto;
import com.vra.core.base.constants.CoreSecurityConstants;
import com.vra.core.base.exception.CoreException;
import com.vra.core.base.exception.CoreHttpException;
import com.vra.core.security.context.SecurityContextHolder;
import com.vra.core.security.helper.UserMicroserviceHelper;
import com.vra.core.security.service.AuthService;
import com.vra.core.user.request.dto.ResendOtpRequestDto;
import com.vra.core.user.request.dto.UserLoginRequestDto;
import com.vra.core.user.request.dto.ValidateUserOtpRequestDto;
import com.vra.core.user.response.dto.UserLoginResponseDto;
import com.vra.core.user.response.dto.UserResponseDto;

public class AuthServiceImpl implements AuthService {

	private static final Logger logger = LogManager.getLogger(AuthServiceImpl.class);

	private ObjectMapper objectMapper;

	public AuthServiceImpl(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public ResponseDto<UserLoginResponseDto> login(UserLoginRequestDto dto) {
		CoreHttpClient httpClient = new CoreHttpClient(UserMicroserviceHelper.getUrl(UserMicroserviceHelper.USER_LOGIN));

		try {
			String responseString = httpClient.executeMethodPost(objectMapper.writeValueAsString(dto));

			logger.debug(responseString);

			TypeReference<ResponseDto<UserLoginResponseDto>> typeReference = new TypeReference<ResponseDto<UserLoginResponseDto>>() {
			};

			return objectMapper.readValue(responseString, typeReference);

		} catch (IOException e) {
			logger.error("Error while processing HTTP Request: ", e);
			throw new CoreHttpException(e.getMessage(), e);
		}
	}

	@Override
	public ResponseDto<UserLoginResponseDto> validateOtp(ValidateUserOtpRequestDto dto) {
		CoreHttpClient httpClient = new CoreHttpClient(UserMicroserviceHelper.getUrl(UserMicroserviceHelper.LOGIN_OTP_VALIDATION));

		try {
			String responseString = httpClient.executeMethodPost(objectMapper.writeValueAsString(dto));

			logger.debug(responseString);

			TypeReference<ResponseDto<UserLoginResponseDto>> typeReference = new TypeReference<ResponseDto<UserLoginResponseDto>>() {
			};

			return objectMapper.readValue(responseString, typeReference);

		} catch (IOException e) {
			logger.error("Error while processing HTTP Request: ", e);
			throw new CoreHttpException(e.getMessage(), e);
		}
	}

	@Override
	public ResponseDto<Void> resendOtp(ResendOtpRequestDto resendOtpRequestDto) {
		CoreHttpClient httpClient = new CoreHttpClient(UserMicroserviceHelper.getUrl(UserMicroserviceHelper.RESEND_LOGIN_OTP));

		try {
			String responseString = httpClient.executeMethodPost(objectMapper.writeValueAsString(resendOtpRequestDto));

			logger.debug(responseString);

			TypeReference<ResponseDto<Void>> typeReference = new TypeReference<ResponseDto<Void>>() {
			};

			return objectMapper.readValue(responseString, typeReference);

		} catch (IOException e) {
			logger.error("Error while processing HTTP Request: ", e);
			throw new CoreHttpException(e.getMessage(), e);
		}
	}

	@Override
	public ResponseDto<UserResponseDto> getCurrentUser() {
		CoreHttpClient httpClient = new CoreHttpClient(UserMicroserviceHelper.getUrl(UserMicroserviceHelper.GET_CURRENT_USER_PROFILE));

		httpClient.addHeader(CoreSecurityConstants.TOKEN_HEADER_NAME, SecurityContextHolder.getCurrentUser().getToken());

		String responseString = httpClient.executeMethodGet();

		TypeReference<ResponseDto<UserResponseDto>> typeReference = new TypeReference<ResponseDto<UserResponseDto>>() {
		};

		try {

			return objectMapper.readValue(responseString, typeReference);

		} catch (IOException e) {
			logger.error("Error while processing HTTP Request: ", e);
			throw new CoreHttpException(e.getMessage(), e);
		}
	}

	@Override
	public ResponseDto<Void> logout() {
		CoreHttpClient httpClient = new CoreHttpClient(UserMicroserviceHelper.getUrl(UserMicroserviceHelper.SIGN_OUT));

		httpClient.addHeader(CoreSecurityConstants.TOKEN_HEADER_NAME, SecurityContextHolder.getCurrentUser().getToken());

		String responseString = httpClient.executeMethodGet();

		TypeReference<ResponseDto<Void>> typeReference = new TypeReference<ResponseDto<Void>>() {
		};

		try {
			return objectMapper.readValue(responseString, typeReference);

		} catch (IOException e) {
			logger.error("Error while processing HTTP Request: ", e);
			throw new CoreHttpException(e.getMessage(), e);
		}
	}

	@Override
	public ResponseDto<UserResponseDto> validateToken(String token) {
		CoreHttpClient httpClient = new CoreHttpClient(UserMicroserviceHelper.getUrl(UserMicroserviceHelper.VALIDATE_TOKEN));

		httpClient.addHeader(CoreSecurityConstants.TOKEN_HEADER_NAME, token);

		String responseString = httpClient.executeMethodGet();

		TypeReference<ResponseDto<UserResponseDto>> typeReference = new TypeReference<ResponseDto<UserResponseDto>>() {
		};

		try {
			return objectMapper.readValue(responseString, typeReference);

		} catch (IOException e) {
			logger.error("Error while processing HTTP Request: ", e);
			throw new CoreHttpException(e.getMessage(), e);
		}
	}

	@Override
	public void validateUrlPermission(String userId, String uri) {
		CoreHttpClient httpClient = new CoreHttpClient(UserMicroserviceHelper.getUrl(UserMicroserviceHelper.VALIDATE_PERMISSION));

		httpClient.addHeader(CoreSecurityConstants.USER_ID_CAMALCASE, userId);
		httpClient.addHeader(CoreSecurityConstants.URI, uri);

		try (CloseableHttpResponse httpResponse = httpClient.executeGet()) {
			StatusLine statusLine = httpResponse.getStatusLine();

			if (statusLine.getStatusCode() != 200) {
				throw new CoreException(statusLine.getReasonPhrase(), statusLine.getStatusCode());
			}
		} catch (IOException e) {
			logger.error("Error while processing HTTP Request: ", e);
			throw new CoreHttpException(e.getMessage(), e);
		}
	}

}