package com.vra.core.security.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vra.core.base.common.ResponseDto;
import com.vra.core.base.constants.CoreSecurityConstants;
import com.vra.core.base.utils.SecureCookieUtil;
import com.vra.core.security.helper.UserMicroserviceHelper;
import com.vra.core.security.service.AuthService;
import com.vra.core.security.service.impl.AuthServiceImpl;
import com.vra.core.user.enums.UserType;
import com.vra.core.user.request.dto.ResendOtpRequestDto;
import com.vra.core.user.request.dto.UserLoginRequestDto;
import com.vra.core.user.request.dto.ValidateUserOtpRequestDto;
import com.vra.core.user.response.dto.UserLoginResponseDto;
import com.vra.core.user.response.dto.UserResponseDto;

public class UserBasicOperationController {

	private static final Logger logger = LogManager.getLogger(UserBasicOperationController.class);

	private AuthService authService;
	private List<UserType> supportedUserTypes;

	public UserBasicOperationController(
			@NotNull ObjectMapper objectMapper,
			@NotNull String authServiceUrl,
			@NotNull String userServiceUrl,
			UserType... supportedUserTypes) {

		if (supportedUserTypes != null) {
			this.supportedUserTypes = Arrays.asList(supportedUserTypes);
		}

		UserMicroserviceHelper.configure(authServiceUrl, userServiceUrl);
		this.authService = new AuthServiceImpl(objectMapper);
	}

	@PostMapping(path = "user/login")
	public ResponseDto<UserLoginResponseDto> login(@RequestBody @Valid UserLoginRequestDto userLoginRequestDto, HttpServletResponse response) {
		logger.debug("Login Request for User: " + userLoginRequestDto.getUsername());

		if (CollectionUtils.isNotEmpty(supportedUserTypes) && !supportedUserTypes.contains(userLoginRequestDto.getUserType())) {
			return new ResponseDto<>(false, "Invalid user type");
		}

		ResponseDto<UserLoginResponseDto> userResponse = authService.login(userLoginRequestDto);

		if (userResponse.isStatus() && userResponse.getData() != null) {
			addTokenToResponse(response, userResponse.getData());
		}

		return userResponse;
	}

	@PostMapping(path = "user/otp/validate")
	public ResponseDto<UserLoginResponseDto> validateOtp(@RequestBody @Valid ValidateUserOtpRequestDto validateUserOtpRequestDto, HttpServletResponse response) {
		logger.debug("OTP Validation Request For User: " + validateUserOtpRequestDto.getUsername());

		if (CollectionUtils.isNotEmpty(supportedUserTypes) && !supportedUserTypes.contains(validateUserOtpRequestDto.getUserType())) {
			return new ResponseDto<>(false, "Invalid user type");
		}

		ResponseDto<UserLoginResponseDto> userResponse = authService.validateOtp(validateUserOtpRequestDto);

		if (userResponse.isStatus() && userResponse.getData() != null) {
			addTokenToResponse(response, userResponse.getData());
		}

		return userResponse;
	}

	private void addTokenToResponse(HttpServletResponse response, UserLoginResponseDto userResponse) {

		if (StringUtils.isNotBlank(userResponse.getToken())) {
			response.addHeader(CoreSecurityConstants.TOKEN_HEADER_NAME, userResponse.getToken());
			response.addCookie(SecureCookieUtil.create(CoreSecurityConstants.TOKEN_HEADER_NAME, userResponse.getToken()));
		}
	}

	@PostMapping(path = "user/otp/resend")
	public ResponseDto<Void> resendOtp(@RequestBody @Valid ResendOtpRequestDto resendOtpRequestDto, HttpServletResponse response) {
		logger.debug("Resend OTP Request For User: " + resendOtpRequestDto.getUsername());

		return authService.resendOtp(resendOtpRequestDto);
	}

	@GetMapping(path = "user/logout")
	public ResponseDto<Void> logout(HttpServletRequest request, HttpServletResponse response) {
		ResponseDto<Void> responseDto = authService.logout();

		SecureCookieUtil.handleLogOutResponse(request, response);

		return responseDto;
	}

	@GetMapping(path = "user/getProfile")
	public ResponseDto<UserResponseDto> getUserProfile() {
		return authService.getCurrentUser();

	}

}