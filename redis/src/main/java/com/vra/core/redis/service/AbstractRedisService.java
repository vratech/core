package com.vra.core.redis.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.redis.core.RedisTemplate;

import com.vra.core.redis.dto.AbstractRedisDto;

public interface AbstractRedisService<T extends AbstractRedisDto, I extends Serializable> {

	T save(T dto);

	T update(T dto);

	Iterable<T> save(List<T> dtos);

	Iterable<T> update(List<T> dtos);

	T find(I id);

	Iterable<T> find(List<I> ids);

	Iterable<T> findAll();

	List<T> findList(List<I> ids);

	List<T> findAllList();

	void delete(T dto);

	void delete(List<T> dtos);

	void delete(I id);

	void deleteAll();

	RedisTemplate<? extends Serializable, ? extends Serializable> getRedisTemplate();

}