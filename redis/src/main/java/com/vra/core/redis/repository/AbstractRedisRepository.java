package com.vra.core.redis.repository;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.vra.core.redis.dto.AbstractRedisDto;

@NoRepositoryBean
public interface AbstractRedisRepository<T extends AbstractRedisDto, I extends Serializable> extends CrudRepository<T, I> {

}