package com.vra.core.redis.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractJpaRedisDto extends AbstractRedisDto {

	protected String uuid;

	public AbstractJpaRedisDto(
			String id,
			String uuid,
			Date createdAt,
			Date updatedAt,
			String createdBy,
			String updatedBy,
			Boolean isDeleted,
			Boolean isPublished) {
		
		super(id, createdAt, updatedAt, createdBy, updatedBy, isDeleted, isPublished);
		
		this.uuid = uuid;
	}

}