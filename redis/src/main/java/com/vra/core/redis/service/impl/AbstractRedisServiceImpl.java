package com.vra.core.redis.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.annotation.Resource;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.core.RedisTemplate;

import com.vra.core.redis.dto.AbstractRedisDto;
import com.vra.core.redis.repository.AbstractRedisRepository;
import com.vra.core.redis.service.AbstractRedisService;

public abstract class AbstractRedisServiceImpl<T extends AbstractRedisDto, I extends Serializable, R extends AbstractRedisRepository<T, I>>
		implements AbstractRedisService<T, I> {

	private static final Logger logger = LogManager.getLogger(AbstractRedisServiceImpl.class);

	protected int limit = 10000;

	@Resource(name = "redisTemplate")
	private RedisTemplate<? extends Serializable, ? extends Serializable> redisTemplate;

	protected abstract R getRepository();

	@Override
	public T save(T redisDto) {
		try {

			return getRepository().save(preSave(redisDto));
		} catch (RedisConnectionFailureException failureException) {
			logger.error("Error is saving [" + redisDto.getId() + "]");
			return null;
		}
	}

	@Override
	public final T update(T redisDto) {
		try {
			return getRepository().save(preUpdate(redisDto));
		} catch (RedisConnectionFailureException failureException) {
			logger.error("Error is updating [" + redisDto.getId() + "]");
			return null;
		}
	}

	@Override
	public Iterable<T> save(List<T> redisDtos) {
		try {
			return getRepository().saveAll(redisDtos.stream().map(this::preSave).collect(Collectors.toList()));
		} catch (RedisConnectionFailureException failureException) {
			logger.error("Error is saving [" + redisDtos.parallelStream()
					.map(T::getId)
					.collect(Collectors.toList()) + "]");
			return null;
		}
	}

	@Override
	public final Iterable<T> update(List<T> redisDtos) {
		try {
			return getRepository().saveAll(redisDtos.stream().map(this::preUpdate).collect(Collectors.toList()));
		} catch (RedisConnectionFailureException failureException) {
			logger.error("Error is updating [" + redisDtos.parallelStream()
					.map(T::getId)
					.collect(Collectors.toList()) + "]");
			return null;
		}
	}

	@Override
	public final T find(I id) {
		try {
			return getRepository().findById(id).orElse(null);
		} catch (RedisConnectionFailureException e) {
			logger.error(e);
			return null;
		}

	}

	@Override
	public final Iterable<T> find(List<I> ids) {
		try {
			return getRepository().findAllById(ids);
		} catch (RedisConnectionFailureException e) {
			logger.error(e);
			return new ArrayList<>();
		}

	}

	@Override
	public final List<T> findList(List<I> ids) {
		try {
			return StreamSupport.stream(getRepository().findAllById(ids).spliterator(), false).collect(Collectors.toList());
		} catch (RedisConnectionFailureException e) {
			logger.error(e);
			return new ArrayList<>();
		}

	}

	@Override
	public final Iterable<T> findAll() {
		try {
			return getRepository().findAll();
		} catch (RedisConnectionFailureException e) {
			logger.error(e);
			return new ArrayList<>();
		}

	}

	@Override
	public final List<T> findAllList() {
		try {
			return StreamSupport.stream(getRepository().findAll().spliterator(), false).collect(Collectors.toList());
		} catch (RedisConnectionFailureException e) {
			logger.error(e);
			return new ArrayList<>();
		}

	}

	@Override
	public final void delete(T index) {
		try {
			getRepository().delete(preDelete(index));
		} catch (RedisConnectionFailureException failureException) {
			logger.debug(failureException);
		}

	}

	@Override
	public final void delete(List<T> redisDtos) {
		try {
			getRepository().deleteAll(redisDtos.stream().map(this::preDelete).collect(Collectors.toList()));
		} catch (RedisConnectionFailureException failureException) {
			logger.debug(failureException);
		}
	}

	@Override
	public void delete(I id) {
		try {
			getRepository().deleteById(id);
		} catch (RedisConnectionFailureException e) {
			logger.error(e);
		}

	}

	@Override
	public final void deleteAll() {
		try {
			getRepository().deleteAll();
		} catch (RedisConnectionFailureException e) {
			logger.error(e);
		}

	}

	private T preSave(T index) {
		if (index.getId() == null || index.getId() == "") {
			index.setId(index.createId());
		}
		if (index.getIsDeleted() == null) {
			index.setIsDeleted(false);
		}
		if (index.getIsPublished() == null) {
			index.setIsPublished(true);
		}
		Date date = new Date();
		if (index.getCreatedAt() == null) {
			index.setCreatedAt(date);
		} else {
			date = new Date(index.getCreatedAt().getTime());
			index.setCreatedAt(date);
		}
		if (index.getUpdatedAt() == null) {
			index.setUpdatedAt(date);
		} else {
			date = new Date(index.getUpdatedAt().getTime());
			index.setUpdatedAt(date);
		}
		return index;
	}

	private T preUpdate(T redisDto) {
		Date date = new Date();
		redisDto.setUpdatedAt(date);
		return redisDto;
	}

	private T preDelete(T redisDto) {
		if (redisDto.getId() == null || redisDto.getId() == "") {
			redisDto.setId(redisDto.createId());
		}
		return redisDto;
	}

	public int getLIMIT() {
		return limit;
	}

	public void setLIMIT(int lIMIT) {
		limit = lIMIT;
	}

	@Override
	public RedisTemplate<? extends Serializable, ? extends Serializable> getRedisTemplate() {
		return redisTemplate;
	}
}