package com.vra.core.redis.dto;

import java.util.Date;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public abstract class AbstractRedisDto {

	protected @Id String id;
	protected Date createdAt;
	protected Date updatedAt;
	protected String createdBy;
	protected String updatedBy;
	protected Boolean isDeleted;
	protected Boolean isPublished;

	public abstract String createId();

}