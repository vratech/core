package com.vra.core.base.common;

import java.util.ArrayDeque;
import java.util.Deque;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.UtilityClass;

/**
 * A thread safe, multi level time measuring utility class.
 * 
 * To call it say:
 * Timer.timeIt("Time taken in my suspected code area");
 * 
 * Then some where down the code say:
 * Timer.timeUp();
 * 
 * It will print time taken from last start point along with the message.
 * 
 * You can call Timer.timeIt() many times inside another child functions etc.
 * Only corresponding time durations will get printed
 * 
 * Many threads can call these functions, the timing calculations do not clash with each other.
 * 
 * There are more elegant/short cut ways of achieving this for example writing
 * an aspect with around advice to all methods. But this method give quick and fine grained
 * time measurement.
 * 
 * This class itself is taking around 10 ms delay for its internal processing.
 * This is due to storing/retrieving timings in a stack.
 * This can be future improvement
 * 
 */
@UtilityClass
public class Timeit {

	private static final Logger logger = LogManager.getLogger(Timeit.class);

	private static ThreadLocal<Deque<StackEntry>> threadLocal = new ThreadLocal<>();

	private static Deque<StackEntry> getStack() {
		if (threadLocal.get() == null) {
			threadLocal.set(new ArrayDeque<StackEntry>());
		}

		return threadLocal.get();
	}

	public static void timeIt() {
		timeIt("");
	}

	public static void timeIt(String message) {
		StackEntry entry = new StackEntry(0, message);
		entry.setTime(System.currentTimeMillis());
		getStack().push(entry);
	}

	public static void timeUp() {
		if (!getStack().isEmpty()) {
			long now = System.currentTimeMillis();
			StackEntry stackEntry = getStack().pop();
			String message = Thread.currentThread().getName() + ": TIME: " + stackEntry.getMessage() + ": " + (now - stackEntry.getTime()) + " milliseconds";
			logger.debug(message);
		}
	}

	// Can be handy while writing performance unit tests or bench mark tests
	public static long timeTaken() {

		if (!getStack().isEmpty()) {

			long now = System.currentTimeMillis();
			StackEntry stackEntry = getStack().pop();
			return now - stackEntry.getTime();
		}

		return 0;
	}

	@Getter
	@Setter
	@AllArgsConstructor
	private static class StackEntry {

		long time;
		String message;

	}
}