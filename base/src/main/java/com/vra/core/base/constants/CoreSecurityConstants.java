/**
 * 
 */
package com.vra.core.base.constants;

import lombok.experimental.UtilityClass;

/**
 * @author naveen
 *
 * @date 28-Oct-2018
 */
@UtilityClass
public class CoreSecurityConstants {

	public static final String TOKEN_HEADER_NAME = "token";
	public static final String USER_ID_CAMALCASE = "userId";
	public static final String URI = "uri";
	public static final String AUTH_TOKEN_HEADER_NAME = "AuthToken";

}