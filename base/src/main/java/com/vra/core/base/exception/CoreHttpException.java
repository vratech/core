/**
 * 
 */
package com.vra.core.base.exception;

/**
 * 
 * @author naveen
 *
 * @date 23-Dec-2018
 */
public class CoreHttpException extends RuntimeException {

	private static final long serialVersionUID = -3368655266237942363L;

	public CoreHttpException() {
		super();
	}

	public CoreHttpException(String message, Throwable cause) {
		super(message, cause);
	}

	public CoreHttpException(String message) {
		super(message);
	}

	public CoreHttpException(Throwable cause) {
		super(cause);
	}
}