package com.vra.core.base.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.experimental.UtilityClass;

@UtilityClass
public class SecureCookieUtil {

	public static Cookie create(String key, String value) {
		Cookie cookie = new Cookie(key, value);
		cookie.setSecure(true);
		cookie.setHttpOnly(true);
		cookie.setPath("/");
		return cookie;
	}

	public static Cookie expire(Cookie cookie) {
		cookie.setMaxAge(0);
		cookie.setValue(null);
		cookie.setPath("/");
		return cookie;
	}

	public static void handleLogOutResponse(HttpServletRequest request, HttpServletResponse response) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				response.addCookie(SecureCookieUtil.expire(cookie));
			}
		}
	}

}