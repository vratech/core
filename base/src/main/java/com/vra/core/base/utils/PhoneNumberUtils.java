package com.vra.core.base.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

import lombok.experimental.UtilityClass;

@UtilityClass
public class PhoneNumberUtils {

	private static final Logger logger = LogManager.getLogger(PhoneNumberUtils.class);

	public static boolean isValidMobileByCountryCode(String mobile, String countryCode) {
		if (StringUtils.isBlank(mobile) || StringUtils.isBlank(countryCode)) {
			return false;
		}

		PhoneNumberUtil util = getPhoneNumberUtil();
		try {
			String isoCode = util.getRegionCodeForCountryCode(Integer.valueOf(PhoneNumberUtil.normalizeDigitsOnly(countryCode)));
			PhoneNumber ph = util.parse(PhoneNumberUtil.normalizeDigitsOnly(mobile), isoCode);

			return util.isValidNumber(ph);

		} catch (NumberParseException e) {
			logger.error("Invalid number encountered while validating number: " + mobile + " for country: " + countryCode, e);
		}

		return false;
	}

	private static PhoneNumberUtil getPhoneNumberUtil() {
		return PhoneNumberUtil.getInstance();
	}
}