package com.vra.core.base.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.vra.core.base.constants.CoreConstants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class DateUtil {

	/**
	 * Method to generate a list of all LocalDate between given date range. It includes the startDate and endDate both in list
	 * 
	 * @param startDate
	 * @param endDate
	 * @return list of LocalDate including start and end date
	 */
	public static List<LocalDate> getAllLocalDatesForRange(LocalDate startDate, LocalDate endDate) {

		List<LocalDate> dates = new ArrayList<>();

		while (startDate.isBefore(endDate) || startDate.isEqual(endDate)) {
			dates.add(startDate);
			startDate = startDate.plusDays(1);
		}

		return dates;
	}

	/**
	 * Method to generate a list of all LocalDate between given date range. It includes the startDate and excludes the endDate in list
	 * 
	 * @param startDate
	 * @param endDate
	 * @return list of LocalDate including startDate and excluding endDate
	 */
	public static List<LocalDate> getAllLocalDatesBetweenRange(LocalDate startDate, LocalDate endDate) {

		List<LocalDate> dates = new ArrayList<>();

		while (!startDate.isEqual(endDate)) {
			dates.add(startDate);
			startDate = startDate.plusDays(1);
		}
		return dates;
	}

	/**
	 * Method to generate a list of all Date between given date range. It includes the startDate and endDate both in list
	 * 
	 * @param startDate
	 * @param endDate
	 * @return list of Date including start and end date
	 */
	public static List<Date> getAllDatesForRange(LocalDate startDate, LocalDate endDate) {
		List<Date> dates = new ArrayList<>();

		while (startDate.isBefore(endDate) || startDate.isEqual(endDate)) {
			dates.add(DateUtil.convertToDate(startDate));
			startDate = startDate.plusDays(1);
		}

		return dates;
	}

	public static Date convertToDate(LocalDate localdate) {
		return Date.from(localdate.atStartOfDay(ZoneId.of(CoreConstants.IST_TIMEZONE)).toInstant());
	}

	public static Date convertToDate(LocalDateTime localdateTime) {
		return Date.from(localdateTime.atZone(ZoneId.of(CoreConstants.IST_TIMEZONE)).toInstant());
	}

	public static LocalDate convertToLocalDate(Date date) {
		if (date == null) {
			return null;
		}

		if (date instanceof java.sql.Date) {
			return ((java.sql.Date) date).toLocalDate();
		}

		ZoneId zoneId = ZoneId.of(CoreConstants.IST_TIMEZONE);
		Instant instant = date.toInstant();

		return instant.atZone(zoneId).toLocalDate();
	}

	public static LocalDateTime convertToLocalDateTime(Date date) {
		if (date == null) {
			return null;
		}

		ZoneId zoneId = ZoneId.of(CoreConstants.IST_TIMEZONE);
		Instant instant = date.toInstant();

		return instant.atZone(zoneId).toLocalDateTime();
	}

	public static long daysBetween(Date start, Date end) {
		long difference = ((start.getTime() - end.getTime()) / CoreConstants.MILLI_SECONDS_IN_DAY);
		return Math.abs(difference);
	}

	public static int getMaxDaysInMonth(LocalDate date) {
		return date.lengthOfMonth();
	}
}