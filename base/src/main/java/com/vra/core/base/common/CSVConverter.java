/**
 * 
 */
package com.vra.core.base.common;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import lombok.experimental.UtilityClass;

/**
 * 
 * @author naveen
 *
 * @date 23-Dec-2018
 */
@UtilityClass
public class CSVConverter {

	/**
	 * Converts Long or Integer List of ids to csv format
	 * 
	 * @param list
	 *            List of Ids
	 * @return csv String
	 */
	@SuppressWarnings("rawtypes")
	public static final String getCSVString(List list) {
		String csvString = "";

		if (CollectionUtils.isNotEmpty(list)) {

			StringBuilder csvSB = new StringBuilder();

			for (int i = 0; i < list.size(); i++) {
				csvSB.append(list.get(i));
				csvSB.append(",");
			}

			csvString = csvSB.toString();
			csvString = csvString.substring(0, csvString.length() - 1);
		}

		return csvString;
	}

	/**
	 * Converts Array of long to csv format
	 * 
	 * @param list
	 *            Long List of Ids
	 * @return csv String
	 */
	public static final String getCSVString(long[] ids) {
		String csvString = "";

		if (ids != null && ids.length > 0) {

			StringBuilder csvSB = new StringBuilder();

			for (int i = 0; i < ids.length; i++) {
				csvSB.append(ids[i]);
				csvSB.append(',');
			}

			csvString = csvSB.toString();
			csvString = csvString.substring(0, csvString.length() - 1);
		}

		return csvString;
	}

	/**
	 * Converts Array of int to csv format
	 * 
	 * @param int
	 *            array of ids
	 * @return csv String
	 */
	public static final String getCSVString(int[] ids) {
		String csvString = "";

		if (ids != null && ids.length > 0) {

			StringBuilder csvSB = new StringBuilder();

			for (int i = 0; i < ids.length; i++) {
				csvSB.append(ids[i]);
				csvSB.append(',');
			}

			csvString = csvSB.toString();
			csvString = csvString.substring(0, csvString.length() - 1);
		}

		return csvString;
	}

	/**
	 * Converts Array of String ids to csv format
	 * 
	 * @param list
	 *            Long List of Ids
	 * @return csv String
	 */
	public static final String getCSVString(String[] ids) {
		int length = (ids == null) ? 0 : ids.length;
		return getCSVString(ids, 0, length);
	}

	public static final String getCSVString(String[] ids, int startIndex, int endIndex) {
		String csvString = "";

		if (ids != null && ids.length > 0) {

			StringBuilder csvSB = new StringBuilder();

			for (int i = startIndex; i < endIndex; i++) {
				csvSB.append('\'');
				csvSB.append(ids[i]);
				csvSB.append('\'');
				csvSB.append(',');
			}
			csvString = csvSB.toString();
			csvString = csvString.substring(0, csvString.length() - 1);
		}

		return csvString;
	}

}