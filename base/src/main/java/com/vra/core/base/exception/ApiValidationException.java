/**
 * 
 */
package com.vra.core.base.exception;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(callSuper = true)
public class ApiValidationException extends RuntimeException {

	private static final long serialVersionUID = -4075250719863628707L;

	private final String code;

	public ApiValidationException(String message) {
		this(message, null, null);
	}

	public ApiValidationException(Throwable cause) {
		this(null, null, cause);
	}

	public ApiValidationException(String message, String code) {
		this(message, code, null);
	}

	public ApiValidationException(String message, Throwable cause) {
		this(message, null, cause);
	}

	public ApiValidationException(String message, String code, Throwable error) {
		super(message, error);
		this.code = code;
	}

}