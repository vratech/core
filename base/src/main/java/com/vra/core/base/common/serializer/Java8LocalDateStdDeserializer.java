/**
 * 
 */
package com.vra.core.base.common.serializer;

import java.io.IOException;
import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * @author naveen
 *
 * @date 01-Oct-2018
 */
public class Java8LocalDateStdDeserializer extends StdDeserializer<LocalDate> {

	private static final long serialVersionUID = 1L;

	public Java8LocalDateStdDeserializer() {
		this(LocalDate.class);
	}

	public Java8LocalDateStdDeserializer(Class<LocalDate> t) {
		super(t);
	}

	@Override
	public LocalDate deserialize(JsonParser jsonparser, DeserializationContext context)
			throws IOException {
		return LocalDate.parse(jsonparser.getText());
	}
}