package com.vra.core.base.common;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@NoArgsConstructor
public class ResponseDto<T> {

	private boolean status;
	private String message;
	private T data;
	private String id;

	public ResponseDto(T data) {
		this.data = data;
	}

	public ResponseDto(boolean status) {
		this.status = status;
	}

	public ResponseDto(boolean status, String message) {
		this(status);
		this.message = message;
	}

	public ResponseDto(boolean status, String message, T data) {
		this(status, message);
		this.data = data;
	}

	public ResponseDto(boolean status, String message, T data, String id) {
		this(status, message, data);
		this.id = id;
	}

	public static <T> ResponseDto<T> success(String message) {
		return new ResponseDto<>(true, message, null);
	}

	public static <T> ResponseDto<T> success(String message, T data) {
		return new ResponseDto<>(true, message, data);
	}

	public static <T> ResponseDto<T> failure(String message) {
		return new ResponseDto<>(false, message);
	}

	public static <T> ResponseDto<T> failure(String message, String id) {
		return new ResponseDto<>(false, message, null, id);
	}
}