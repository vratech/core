package com.vra.core.base.exception;

import lombok.Getter;
import lombok.ToString;

/**
 * 
 * @author naveen
 *
 * @date 27-Oct-2018
 */
@Getter
@ToString(callSuper = true)
public class CoreException extends RuntimeException {

	private static final long serialVersionUID = 54730819L;

	private final String code;
	private final int statusCode;

	public CoreException(String message) {
		this(message, null, null);
	}

	public CoreException(Throwable cause) {
		this(null, null, cause);
	}

	public CoreException(String message, String code) {
		this(message, code, null);
	}

	public CoreException(String message, int statusCode) {
		this(message, null, null, statusCode);
	}

	public CoreException(String message, Throwable cause) {
		this(message, null, cause);
	}

	public CoreException(String message, String code, Throwable error) {
		this(message, code, error, -1);
	}

	public CoreException(String message, String code, Throwable error, int statusCode) {
		super(message, error);
		this.code = code;
		this.statusCode = statusCode;
	}

}