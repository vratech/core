/**
 * 
 */
package com.vra.core.base.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.vra.core.base.exception.CoreHttpException;

import lombok.experimental.UtilityClass;

/**
 * 
 * @author naveen
 *
 * @date 27-Oct-2018
 */
public class CoreHttpClient {

	private static final Logger logger = LogManager.getLogger(CoreHttpClient.class);

	static final String POST_RESPONSE_STATUS = "POST Response Status:: ";
	static final String POST_REQUEST_EXECUTION_ERROR = "Error while executing POST Http request with URL: ";
	static final String HTTP_REQUEST_EXECUTION_FAIL = "Failed to execute the Http POST request: ";
	static final String FOR_URL = " for URL:: ";

	private String url;
	private List<NameValuePair> urlParameters = new ArrayList<>();
	private Map<String, String> headers = new HashMap<>();

	public CoreHttpClient() {
	}

	public CoreHttpClient(String url) {
		this.url = url;
	}

	public CoreHttpClient(String url, Map<String, String> headers) {
		this.url = url;
		if (headers != null) {
			this.headers.putAll(headers);
		}
	}

	public void addHeader(String key, String value) {
		this.headers.put(key, value);
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void addUrlParameters(List<NameValuePair> urlParameters) {
		if (urlParameters != null) {
			this.urlParameters.addAll(urlParameters);
		}
	}

	/**
	 * Method to add new url parameter in the http request
	 * 
	 * @param param
	 */
	public void addUrlParamter(String name, String value) {
		urlParameters.add(new BasicNameValuePair(name, value));
	}

	/**
	 * Utility method to execute the HTTP request taking URL parameters as POST data
	 * 
	 * @return http response string
	 * @throws CoreHttpException
	 */
	public String executeMethodPost() throws CoreHttpException {
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpEntity postParams = new UrlEncodedFormEntity(urlParameters);

			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(postParams);

			CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
			logger.info(POST_RESPONSE_STATUS + httpResponse.getStatusLine().getStatusCode());
			return parseResponse(httpResponse);
		} catch (IOException e) {
			logger.error(POST_REQUEST_EXECUTION_ERROR + url + " and params: " + Arrays.toString(urlParameters.toArray()), e);
			throw new CoreHttpException(HTTP_REQUEST_EXECUTION_FAIL, e);
		}
	}

	/**
	 * Utility method to execute the HTTP request taking JSON object as POST data
	 * 
	 * @param jsonString
	 *            - json object string to be sent as POST data
	 * @return http response string
	 * @throws CoreHttpException
	 */
	public String executeMethodPost(String jsonString) throws CoreHttpException {

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			StringEntity postParams = new StringEntity(jsonString, ContentType.APPLICATION_JSON);
			HttpPost httpPost = new HttpPost(url);

			addHeaders(httpPost);

			httpPost.setEntity(postParams);

			CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
			logger.info(POST_RESPONSE_STATUS + httpResponse.getStatusLine().getStatusCode() + FOR_URL + url);

			return parseResponse(httpResponse);
		} catch (IOException e) {
			logger.error(POST_REQUEST_EXECUTION_ERROR + url + " and params: " + jsonString, e);
			throw new CoreHttpException(HTTP_REQUEST_EXECUTION_FAIL, e);
		}
	}

	/**
	 * Utility method to execute the HTTP request taking JSON object as POST data
	 * 
	 * @param jsonString
	 *            - json object string to be sent as POST data
	 * @return http response string
	 * @throws CoreHttpException
	 */
	public String executeMethodPost(InputStream is) throws CoreHttpException {

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

			InputStreamEntity postParams = new InputStreamEntity(is, ContentType.APPLICATION_JSON);

			HttpPost httpPost = new HttpPost(url);

			addHeaders(httpPost);

			httpPost.setEntity(postParams);

			CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
			logger.info(POST_RESPONSE_STATUS + httpResponse.getStatusLine().getStatusCode() + FOR_URL + url);

			return parseResponse(httpResponse);
		} catch (IOException e) {
			logger.error("POST_REQUEST_EXECUTION_ERROR " + url, e);
			throw new CoreHttpException(HTTP_REQUEST_EXECUTION_FAIL, e);
		}
	}

	/**
	 * Utility method to execute the HTTP GET request
	 * 
	 * @return http response string
	 * @throws CoreHttpException
	 */
	public String executeMethodGet() throws CoreHttpException {
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

			HttpGet httpGet = new HttpGet(url);

			addHeaders(httpGet);

			CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
			logger.info("GET Response Status:: " + httpResponse.getStatusLine().getStatusCode() + FOR_URL + url);
			return parseResponse(httpResponse);
		} catch (IOException e) {
			logger.error("Error while executing GET Http request with URL: " + url, e);
			throw new CoreHttpException("Failed to execute the Http GET request: ", e);
		}
	}

	/**
	 * Utility method to execute the HTTP GET request
	 * 
	 * @return http response string
	 * @throws CoreHttpException
	 */
	public CloseableHttpResponse executeGet() throws CoreHttpException {
		try {

			HttpGet httpGet = new HttpGet(url);

			addHeaders(httpGet);

			CloseableHttpResponse httpResponse = SingletonHttpClient.httpClient.execute(httpGet);
			logger.info("GET Response Status:: " + httpResponse.getStatusLine().getStatusCode() + FOR_URL + url);
			validateResponseStatus(httpResponse);

			return httpResponse;
		} catch (IOException e) {
			logger.error("Error while executing GET Http request with URL: " + url, e);
			throw new CoreHttpException("Failed to execute the Http GET request: ", e);
		}
	}

	private void addHeaders(HttpRequestBase httpGet) {
		headers.forEach(httpGet::setHeader);
	}

	public Pair<String, Integer> executeMethodDelete() throws CoreHttpException {
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

			HttpDelete httpGet = new HttpDelete(url);

			addHeaders(httpGet);

			CloseableHttpResponse httpResponse = httpClient.execute(httpGet);

			logger.info("DELETE Response Status:: " + httpResponse.getStatusLine().getStatusCode() + FOR_URL + url);

			return Pair.of(parseResponse(httpResponse), httpResponse.getStatusLine().getStatusCode());
		} catch (IOException e) {
			logger.error("Error while executing DELETE Http request with URL: " + url, e);
			throw new CoreHttpException("Failed to execute the Http DELETE request: ", e);
		}
	}

	private String parseResponse(CloseableHttpResponse httpResponse) throws IOException, CoreHttpException {

		validateResponseStatus(httpResponse);
		BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
		String inputLine;

		StringBuilder stringBuilder = new StringBuilder();

		while ((inputLine = reader.readLine()) != null) {
			stringBuilder.append(inputLine);
		}

		reader.close();
		return stringBuilder.toString();
	}

	private void validateResponseStatus(CloseableHttpResponse httpResponse) throws CoreHttpException {
		if (httpResponse.getStatusLine().getStatusCode() == 401) {
			throw new CoreHttpException("Invalid token or User Session has expired");
		}

		if (httpResponse.getStatusLine().getStatusCode() == 403) {
			throw new CoreHttpException("Access Denied for user");
		}

		if (httpResponse.getStatusLine().getStatusCode() == 404) {
			throw new CoreHttpException("Service Unavailable! Please try after sometime");
		}
	}

	@UtilityClass
	private static class SingletonHttpClient {

		public static final CloseableHttpClient httpClient;

		static {

			httpClient = HttpClientBuilder
					.create()
					.setMaxConnTotal(100)
					.setMaxConnPerRoute(20)
					.evictIdleConnections(10, TimeUnit.SECONDS)
					.build();

			Runtime.getRuntime().addShutdownHook(new Thread(() -> {
				try {
					httpClient.close();
				} catch (IOException e) {
					logger.error("error while shuting down httpClient", e);
				}
			}, "SingletonHttpClient-Shutdown HttpClient"));
		}

	}

}