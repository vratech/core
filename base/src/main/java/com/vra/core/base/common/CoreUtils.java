package com.vra.core.base.common;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import com.vra.core.base.constants.CoreConstants;

import lombok.experimental.UtilityClass;

/**
 * 
 * @author naveen
 *
 * @date 27-Oct-2018
 */
@UtilityClass
public class CoreUtils {

	/**
	 * Method to hide the Password or any secret string. Converts the secret string into *
	 * 
	 * @param secretString
	 * @return converted secret string into * characters
	 */
	public static String hideSecret(String secretString) {

		if (StringUtils.isNotBlank(secretString)) {
			return "******";
		}

		return StringUtils.EMPTY;
	}

	/**
	 * Returns random 6 digit number(OTP) between 100000-999999
	 * 
	 * @return 6 digit number(OTP)
	 */
	public static int generateOTP() {
		return ThreadLocalRandom.current().nextInt(100000, 1000000);
	}

	/**
	 * Return a random integer number between the given range inclusive @start and excluding @end
	 * 
	 * @param start
	 *            - the smallest random number required
	 * @param end
	 *            - one less than the maximum random number required
	 * @return random integer number between the given range inclusive @start and excluding @end
	 * @throws IllegalArgumentException
	 *             - if start is greater than equal to end
	 */
	public static int getRandomNumberBetweenRange(int start, int end) {
		return ThreadLocalRandom.current().nextInt(start, end);
	}

	/**
	 * Check if Email is valid or not
	 * 
	 * @param email
	 * @return
	 */
	public static boolean isValidEmail(String email) {
		if (StringUtils.isBlank(email)) {
			return false;
		}
		return EmailValidator.getInstance().isValid(email);
	}

	/**
	 * 
	 * @param length
	 * @return a randomly generated UUID
	 */
	public static String generateUniqueId(int length) {
		return RandomStringUtils.randomAlphanumeric(length).toUpperCase();
	}

	/**
	 * A randomly generated unique ID based on UUID
	 * 
	 * @return a randomly generated UUID
	 */
	public static String generateUniqueId() {
		return UUID.randomUUID().toString();
	}

	public static String generateMaskedMobile(long mobile) {
		return "+91" + Long.toString(mobile);
	}

	public static Long generateMaskedMobileNo(String mobile) {

		if (mobile.startsWith("+91")) {
			return Long.parseLong(mobile.substring(3));
		} else if (mobile.startsWith("91")) {
			return Long.parseLong(mobile.substring(2));
		}

		throw new IllegalArgumentException("invalid mobile no");
	}

	public static double roundToPlaces(double value, int places) {
		double numberToDivide = 1;
		while (places > 0) {
			numberToDivide *= 10;
			places--;
		}
		Double number = Double.valueOf(numberToDivide);
		return Math.round(value * number) / number;
	}

	public static double roundOff(Double price) {

		if (price != null) {
			return roundToPlaces(price, CoreConstants.PRICE_ROUND_OFF_DIGITS);
		}

		return 0d;
	}

}