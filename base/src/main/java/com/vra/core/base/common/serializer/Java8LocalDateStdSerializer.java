/**
 * 
 */
package com.vra.core.base.common.serializer;

import java.io.IOException;
import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * @author naveen
 *
 * @date 01-Oct-2018
 */
public class Java8LocalDateStdSerializer extends StdSerializer<LocalDate> {

	private static final long serialVersionUID = 1L;

	public Java8LocalDateStdSerializer() {
		this(LocalDate.class);
	}

	public Java8LocalDateStdSerializer(Class<LocalDate> t) {
		super(t);
	}

	@Override
	public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeObject(value.toString());
	}

}