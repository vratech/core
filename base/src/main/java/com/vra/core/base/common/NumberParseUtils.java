/**
 * 
 */
package com.vra.core.base.common;

import org.apache.commons.lang3.math.NumberUtils;

import lombok.experimental.UtilityClass;

/**
 * 
 * @author naveen
 *
 * @date 23-Dec-2018
 */
@UtilityClass
public class NumberParseUtils {

	public static int getIntValue(String stringValue) {
		return isParsable(stringValue) ? Integer.parseInt(stringValue) : 0;
	}

	public static long getLongValue(String stringValue) {
		return isParsable(stringValue) ? Long.parseLong(stringValue) : 0;
	}

	public static float getFloatValue(String stringValue) {
		return isParsable(stringValue) ? Float.parseFloat(stringValue) : 0;

	}

	public static double getDoubleValue(String stringValue) {
		return isParsable(stringValue) ? Double.parseDouble(stringValue) : 0;

	}

	private static boolean isParsable(String stringValue) {
		return NumberUtils.isParsable(stringValue);
	}
}