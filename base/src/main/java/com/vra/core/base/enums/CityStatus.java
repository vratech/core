package com.vra.core.base.enums;

import lombok.Getter;

@Getter
public enum CityStatus {
	
	ACTIVE(1),
	INACTIVE(0);

	private int statusId;

	CityStatus(int statusId) {
		this.statusId = statusId;
	}
}