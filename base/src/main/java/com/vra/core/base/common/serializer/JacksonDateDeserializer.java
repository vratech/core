package com.vra.core.base.common.serializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class JacksonDateDeserializer extends StdDeserializer<Date> {

	private static final long serialVersionUID = 1L;

	public JacksonDateDeserializer() {
		this(null);
	}

	public JacksonDateDeserializer(Class<?> vc) {
		super(vc);
	}

	@Override
	public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String date = jp.getText();
		try {
			return format.parse(date);
		} catch (ParseException e) {
			throw new IOException(e);
		}

	}
}