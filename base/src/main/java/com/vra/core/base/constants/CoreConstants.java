package com.vra.core.base.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class CoreConstants {

	public static final boolean RESPONSE_STATUS_SUCCESS = true;
	public static final boolean RESPONSE_STATUS_ERROR = false;

	public static final long MILLI_SECONDS_IN_DAY = 86400000;
	public static final int PRICE_ROUND_OFF_DIGITS = 2;

	public static final String IST_TIMEZONE = "Asia/Kolkata";
	public static final String INDIA_COUNTRY_CODE = "91";

}