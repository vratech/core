package com.vra.core.amazons3.service.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.vra.core.amazons3.config.MultipleS3ClientProvider;
import com.vra.core.amazons3.dto.DownloadDto;
import com.vra.core.amazons3.dto.FileStream;
import com.vra.core.amazons3.dto.UploadDto;
import com.vra.core.amazons3.enums.S3Client;
import com.vra.core.amazons3.service.AmazonS3BucketService;
import com.vra.core.base.exception.CoreException;

@Service
public class AmazonS3BucketServiceImpl implements AmazonS3BucketService {

	private static final Logger logger = LogManager.getLogger(AmazonS3BucketServiceImpl.class);
	public static final int MAX_KEYS = 200;

	@Autowired
	private MultipleS3ClientProvider s3ClientProvider;

	@Override
	public void upload(UploadDto dto, S3Client s3Client) {

		InputStream inputstream = new ByteArrayInputStream(dto.getBuffer());
		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentLength(dto.getBuffer().length);
		meta.setContentType(dto.getContentType());
		PutObjectRequest req = new PutObjectRequest(dto.getBucketName(), dto.getPathPrefix() + "/" + dto.getFileName(), inputstream, meta);

		if (dto.isAccessModifcationsIsPrivate()) {
			req = req.withCannedAcl(CannedAccessControlList.Private);
		} else {
			req = req.withCannedAcl(CannedAccessControlList.PublicRead);
		}

		try {

			s3ClientProvider.getConfig(s3Client).putObject(req);

		} catch (Exception e) {
			logger.error("Error uploading file on S3: ", e);
		}

	}

	@Override
	public FileStream download(DownloadDto dto, S3Client s3Client) {
		try {
			GetObjectRequest request = new GetObjectRequest(dto.getBucketName(), dto.getKey());
			S3Object object = s3ClientProvider.getConfig(s3Client).getObject(request);
			return new FileStream(object.getObjectContent(), object.getObjectMetadata().getContentLength());
		} catch (Exception exception) {
			throw new CoreException(exception);
		}
	}

	@Override
	public String generatePreSignedUrl(GeneratePresignedUrlRequest presignedUrlRequest, S3Client s3Client) {
		return s3ClientProvider.getConfig(s3Client).generatePresignedUrl(presignedUrlRequest).toString();
	}

	@Override
	public List<S3ObjectSummary> getListOfObjects(String bucketName, String prefix, S3Client s3Client) {

		List<S3ObjectSummary> summaryList = new ArrayList<>();
		try {
			logger.info("Listing Objects With Prefix: '" + prefix + "'");
			final ListObjectsV2Request req = new ListObjectsV2Request()
					.withBucketName(bucketName)
					.withMaxKeys(MAX_KEYS)
					.withPrefix(prefix);
			ListObjectsV2Result result;
			do {
				result = s3ClientProvider.getConfig(s3Client).listObjectsV2(req);

				summaryList.addAll(result.getObjectSummaries());

				req.setContinuationToken(result.getNextContinuationToken());
			} while (result.isTruncated());

		} catch (AmazonClientException ase) {
			logger.error("Error while listing objects from s3: ", ase);
		}

		return summaryList;
	}
}