package com.vra.core.amazons3.service;

import java.util.List;

import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.vra.core.amazons3.dto.DownloadDto;
import com.vra.core.amazons3.dto.FileStream;
import com.vra.core.amazons3.dto.UploadDto;
import com.vra.core.amazons3.enums.S3Client;

public interface AmazonS3BucketService {

	void upload(UploadDto dto, S3Client s3Client);

	FileStream download(DownloadDto dto, S3Client s3Client);

	String generatePreSignedUrl(GeneratePresignedUrlRequest presignedUrlRequest, S3Client s3Client);

	List<S3ObjectSummary> getListOfObjects(String bucketName, String prefix, S3Client s3Client);
}