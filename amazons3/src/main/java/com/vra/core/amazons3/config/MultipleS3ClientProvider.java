package com.vra.core.amazons3.config;

import com.amazonaws.services.s3.AmazonS3;
import com.vra.core.amazons3.enums.S3Client;

import lombok.Builder;

import java.util.Map;

@Builder
public class MultipleS3ClientProvider {

	private Map<S3Client, AmazonS3> s3ClientsMap;

	public AmazonS3 getConfig(S3Client s3Client) {
		return s3ClientsMap.get(s3Client);
	}

}