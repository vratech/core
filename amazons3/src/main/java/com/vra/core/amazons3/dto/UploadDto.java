package com.vra.core.amazons3.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadDto {

	private String bucketName;

	private String pathPrefix;

	private String fileName;

	private byte[] buffer;

	private String contentType;

	private boolean accessModifcationsIsPrivate;
}