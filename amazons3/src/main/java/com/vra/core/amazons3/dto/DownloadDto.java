package com.vra.core.amazons3.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DownloadDto {

	private String bucketName;

	private String key;

}