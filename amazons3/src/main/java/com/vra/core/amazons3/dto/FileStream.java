package com.vra.core.amazons3.dto;

import com.amazonaws.services.s3.model.S3ObjectInputStream;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileStream {

	private S3ObjectInputStream stream;
	private long length;

	public FileStream(S3ObjectInputStream objectContent, long contentLength) {
		this.stream = objectContent;
		this.length = contentLength;
	}
}